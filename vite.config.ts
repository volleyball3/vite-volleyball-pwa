import { defineConfig } from "vite";
import { VitePWA } from "vite-plugin-pwa";
import react from "@vitejs/plugin-react";
import eslintPlugin from "vite-plugin-eslint";

import { manifestForPlugin } from "./src/manifestForPlugin";

const ICON_BASE_URL = "/vite-volleyball-pwa/";
// const ICON_BASE_URL = "/";

// https://vitejs.dev/config/
export default defineConfig({
  base: "/vite-volleyball-pwa/",
  plugins: [
    react(),
    eslintPlugin(),
    VitePWA(manifestForPlugin(ICON_BASE_URL)),
    // VitePWA(),
  ],
});
