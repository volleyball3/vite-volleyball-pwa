import './App.css';
import { getIsRegistrationOpen } from './utils/helpers';
import { Introduction } from './components';
import { DenseAppBar } from './components';
import { useState } from 'react';
import { menuEntries } from './utils/drawerEntries';

function App() {
  const isRegistrationOpen = getIsRegistrationOpen();
  const [route, setRoute] = useState('');

  return (
    <>
      <DenseAppBar setRoute={setRoute} />
      <Introduction />
      {isRegistrationOpen === 'Open' &&
        menuEntries
          .filter(menuEntry => menuEntry.path === route)
          .map(menuEntry => <menuEntry.component />)}
    </>
  );
}

export default App;
