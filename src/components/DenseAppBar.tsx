import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import IconButton from '@mui/material/IconButton';
import MenuIcon from '@mui/icons-material/Menu';
import FavoriteIcon from '@mui/icons-material/Favorite';
import {
  Drawer,
  List,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  Paper,
} from '@mui/material';
import { menuEntries } from '../utils/drawerEntries';

const DenseAppBar = ({
  setRoute,
}: {
  setRoute: React.Dispatch<React.SetStateAction<string>>;
}) => {
  const [isDrawerOpen, setIsDrawerOpen] = React.useState(false);
  const toggleDrawer = (open: boolean) => {
    setIsDrawerOpen(open);
  };
  const list = () => (
    <Box
      sx={{ width: 250 }}
      role="presentation"
      onClick={() => toggleDrawer(false)}
      onKeyDown={() => toggleDrawer(false)}
    >
      <List>
        {menuEntries.map(menuEntry => (
          <ListItem
            key={menuEntry.name}
            disablePadding
            onClick={() => {
              setRoute(menuEntry.path);
            }}
          >
            <ListItemButton>
              <ListItemIcon>
                <menuEntry.icon />
              </ListItemIcon>
              <ListItemText primary={menuEntry.name} />
            </ListItemButton>
          </ListItem>
        ))}
      </List>
    </Box>
  );

  return (
    <>
      <Box sx={{ display: 'flex' }}>
        <AppBar position="fixed">
          <Toolbar>
            <IconButton
              edge="start"
              color="inherit"
              aria-label="open menu"
              onClick={() => toggleDrawer(true)}
              sx={{ mr: 2 }}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" color="inherit" component="div">
              COE Volleyball
            </Typography>
          </Toolbar>
        </AppBar>
      </Box>
      <Drawer
        anchor={'left'}
        open={isDrawerOpen}
        onClose={() => toggleDrawer(false)}
        sx={{
          width: 250,
        }}
      >
        {list()}
        <Paper
          sx={{
            width: 250,
            height: '100%',
            align: 'center',
          }}
        >
          <Typography
            sx={{
              width: 250,
              position: 'absolute',
              left: 0,
              bottom: '1.25rem',
              fontSize: '0.75rem',
              textAlign: 'center',
            }}
          >
            Made with <FavoriteIcon fontSize="inherit" htmlColor="red" /> by
            <br />
            Akshay Choudhry
          </Typography>
        </Paper>
      </Drawer>
    </>
  );
};

export { DenseAppBar };
