import { useEffect, useState } from 'react';
import { useFirestoreCollection } from '../Registration/useFirestoreCollection';
import {
  Fab,
  Paper,
  TableCell,
  TableContainer,
  tableCellClasses,
  TableHead,
  TableRow,
} from '@mui/material';
import {
  StyledTable,
  StyledTableBody,
  StyledTableRow,
} from '../Registration/RegisteredList/RegisteredList.styles';
import { Shuffle } from '@mui/icons-material';
import styled from '@emotion/styled';
import { log } from '../../utils/helpers';

const shuffle = (array: number[]) => {
  let currentIndex = array.length,
    randomIndex;

  // While there remain elements to shuffle.
  while (currentIndex !== 0) {
    // Pick a remaining element.
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex--;

    // And swap it with the current element.
    [array[currentIndex], array[randomIndex]] = [
      array[randomIndex],
      array[currentIndex],
    ];
  }

  return array;
};

const limitToTeamSizeMap: { [key: string]: number[][] } = {
  '8': [
    [0, 5],
    [5, 9],
  ],
  '9': [
    [0, 5],
    [5, 10],
  ],
  '10': [
    [0, 6],
    [6, 11],
  ],
  '11': [
    [0, 6],
    [6, 12],
  ],
  '12': [
    [0, 7],
    [7, 13],
  ],
  '13': [
    [0, 7],
    [7, 14],
  ],
  '14': [
    [0, 5],
    [5, 10],
    [10, 15],
  ],
  '15': [
    [0, 6],
    [6, 11],
    [11, 16],
  ],
  '16': [
    [0, 6],
    [6, 12],
    [12, 17],
  ],
  '17': [
    [0, 6],
    [6, 12],
    [12, 18],
  ],
};
const randomColor = () =>
  '#' + Math.floor(Math.random() * 16777215).toString(16);

const fabStyle = {
  position: 'fixed',
  bottom: 16,
  right: 16,
};

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: randomColor(),
    color: '#fff',
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const Randomizer = () => {
  const { registeredList } = useFirestoreCollection();
  const [shuffleCounter, setShuffleCounter] = useState(0);
  const [teamsWithNames, setTeamsWithNames] = useState<string[][]>();

  useEffect(() => {
    const limit = registeredList.length < 18 ? registeredList.length - 1 : 17;
    log('limit: ', limit);

    const teamSize = limitToTeamSizeMap[limit];
    log('teamSize: ', teamSize);

    const nNaturalNumbers = Array.from(Array(limit + 1).keys());
    log('nNaturalNumbers: ', nNaturalNumbers);

    const shuffledArray = shuffle(nNaturalNumbers);
    log('shuffledArray: ', shuffledArray);

    const teams =
      teamSize &&
      shuffledArray &&
      teamSize.map(indices => shuffledArray.slice(indices[0], indices[1]));
    log('teams: ', teams);

    const localteamsWithNames =
      teams &&
      teams.map(team =>
        team.map(personIndex => registeredList[personIndex]?.data.name),
      );
    log('localteamsWithNames: ', localteamsWithNames);

    teams && setTeamsWithNames(localteamsWithNames);
  }, [registeredList, shuffleCounter]);

  return registeredList?.length > 8 ? (
    <>
      <TableContainer component={Paper}>
        {teamsWithNames ? (
          <>
            <StyledTable size="small">
              <TableHead>
                <TableRow key="header">
                  {teamsWithNames.map((team, i) => (
                    <StyledTableCell
                      style={{ backgroundColor: randomColor() }}
                      align="center"
                    >
                      {'Team ' + (i + 1)}
                    </StyledTableCell>
                  ))}
                </TableRow>
              </TableHead>
              <StyledTableBody>
                {teamsWithNames[0].map((player, i) =>
                  player ? (
                    <StyledTableRow
                      key={player}
                      sx={{
                        '&:last-child td, &:last-child th': { border: 0 },
                      }}
                    >
                      <StyledTableCell align="center">{player}</StyledTableCell>
                      {teamsWithNames[1] && (
                        <StyledTableCell align="center">
                          {teamsWithNames[1][i] ?? ''}
                        </StyledTableCell>
                      )}
                      {teamsWithNames[2] && (
                        <StyledTableCell align="center">
                          {teamsWithNames[2][i] ?? ''}
                        </StyledTableCell>
                      )}
                    </StyledTableRow>
                  ) : null,
                )}
              </StyledTableBody>
            </StyledTable>
            <br />
          </>
        ) : null}
      </TableContainer>
      <Fab
        sx={fabStyle}
        color="primary"
        aria-label="add"
        onClick={() => setShuffleCounter(shuffleCounter + 1)}
      >
        <Shuffle />
      </Fab>
    </>
  ) : (
    <Paper
      sx={{
        maxWidth: 700,
        marginTop: '1.5rem',
        marginLeft: '.5rem',
        marginRight: '.5rem',
        padding: '.5rem',
        fontSize: '.8rem',
      }}
    >
      Not enough players for a random sample. At least 9 players needed for 2
      teams of 5 and 4.
    </Paper>
  );
};

export { Randomizer };
