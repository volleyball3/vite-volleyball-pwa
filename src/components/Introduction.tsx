import { Paper } from '@mui/material';
import { getIsRegistrationOpen } from '../utils/helpers';

const Weekdays = [
  'Sunday',
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
];

const Introduction = () => {
  const isRegistrationOpen = getIsRegistrationOpen();
  const regDayIndex: number = import.meta.env.VITE_REG_START_DAY;
  const gameDayIndex = (1 + Number(regDayIndex)) % 7;

  return (
    <Paper
      sx={{
        maxWidth: 700,
        marginTop: '4.5rem',
        marginLeft: '.5rem',
        marginRight: '.5rem',
        padding: '.5rem',
        fontSize: '.8rem',
      }}
    >
      <p style={{ fontSize: '1rem' }}>
        <b>
          {isRegistrationOpen === 'Open' && 'Registration Open'}
          {isRegistrationOpen === 'Closed' && 'Registration Closed'}
          {isRegistrationOpen === 'Holiday' &&
            'There will be no volleyball this week. 😢'}
        </b>
      </p>
      <p>
        Volleyball at{' '}
        <a href="https://goo.gl/maps/Bx8x6sRzxsQnysqdA">
          <b>
            <i>Kapellenstraße 36, 96117 Memmelsdorf</i>
          </b>
        </a>{' '}
        on {Weekdays[gameDayIndex]}s.
      </p>
      <p>
        {' '}
        Registration starts every {Weekdays[regDayIndex]} at{' '}
        {import.meta.env.VITE_REG_START_HOUR}:
        {import.meta.env.VITE_REG_START_MINUTES}.
      </p>
      <p>
        After 18 registrations are reached, the rest of the participants are
        added to the waitlist.
      </p>
      <p>
        We leave from COE on {Weekdays[gameDayIndex]}s with our bikes at 19:30
        and play from 20:00 to 22:00.
      </p>
      <p>
        <u>Please remember to pay €2 per player at the end of the game.</u>
      </p>
    </Paper>
  );
};

export { Introduction };
