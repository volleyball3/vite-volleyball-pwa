import { Visibility, VisibilityOff } from '@mui/icons-material';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  IconButton,
  Input,
  InputAdornment,
  InputLabel,
} from '@mui/material';
import { useState } from 'react';

type RegistrationDialogProps = {
  open: boolean;
  handleSubmit: (name: string, pass: string) => void;
  handleClose: () => void;
};

const RegistrationDialog = ({
  open,
  handleSubmit,
  handleClose,
}: RegistrationDialogProps) => {
  const [name, setName] = useState('');
  const [pass, setPass] = useState('');
  const [nameError, setNameError] = useState(false);
  const [passError, setPassError] = useState(false);
  const [showPassword, setShowPassword] = useState(false);

  const validate = () => {
    if (!name.trim()) {
      setNameError(true);
    } else if (!pass) {
      setPassError(true);
    } else {
      handleSubmit(name.trim(), pass);
    }
  };

  const handleClickShowPassword = () => setShowPassword(show => !show);

  const handleMouseDownPassword = (
    event: React.MouseEvent<HTMLButtonElement>,
  ) => {
    event.preventDefault();
  };

  return (
    <Dialog onClose={handleClose} open={open}>
      <DialogTitle>Register</DialogTitle>

      <DialogContent>
        <FormControl variant="standard" sx={{ width: '25ch' }}>
          <InputLabel htmlFor="standard-adornment-name">Name</InputLabel>
          <Input
            autoFocus
            required
            id="standard-adornment-name"
            inputProps={{
              'aria-label': 'name',
            }}
            error={nameError}
            value={name}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
              setNameError(false);
              setName(event.target.value);
            }}
          />
        </FormControl>
        <br />
        <FormControl sx={{ mt: 2, width: '25ch' }} variant="standard">
          <InputLabel htmlFor="standard-adornment-password">
            Password
          </InputLabel>
          <Input
            required
            id="standard-adornment-password"
            type={showPassword ? 'text' : 'password'}
            error={passError}
            value={pass}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
              setPassError(false);
              setPass(event.target.value);
            }}
            autoComplete="current-password"
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={handleClickShowPassword}
                  onMouseDown={handleMouseDownPassword}
                >
                  {showPassword ? <VisibilityOff /> : <Visibility />}
                </IconButton>
              </InputAdornment>
            }
          />
        </FormControl>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Cancel</Button>
        <Button onClick={() => validate()}>Register!</Button>
      </DialogActions>
    </Dialog>
  );
};

export { RegistrationDialog };
