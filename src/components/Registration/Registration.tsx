import { useState } from 'react';
import AddIcon from '@mui/icons-material/Add';
import { Fab } from '@mui/material';

import { RegistrationDialog } from './RegistrationDialog';
import { RegisteredList } from './RegisteredList';
import { DeregistrationDialog } from './DeregistrationDialog';
import { useFirestoreCollection } from './useFirestoreCollection';

const fabStyle = {
  position: 'fixed',
  bottom: 16,
  right: 16,
};

const Registration = () => {
  const [dialogOpen, setDialogOpen] = useState(false);
  const [deregistrationDialog, setDeregistrationDialog] = useState({
    open: false,
    uuid: '',
    pass: '',
  });
  const { registeredList, registerPlayer, deregisterPlayer } =
    useFirestoreCollection();

  const handleClose = () => {
    setDialogOpen(false);
  };

  const handleSubmit = async (name: string, pass: string) => {
    try {
      registerPlayer(name, pass);
      setDialogOpen(false);
    } catch (error) {
      console.log(error);
    }
  };

  const handleDeregistrationClose = () => {
    setDeregistrationDialog({ open: false, uuid: '', pass: '' });
  };

  const handleDeregistrationClick = (uuid: string, pass: string) => {
    setDeregistrationDialog({ open: true, uuid: uuid, pass: pass });
  };

  const handleDeregistration = async (uuid: string, pass: string) => {
    try {
      deregisterPlayer(uuid, pass);
      setDeregistrationDialog({ open: false, uuid: '', pass: '' });
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      {
        <RegisteredList
          registeredList={registeredList}
          handleDeregistrationClick={handleDeregistrationClick}
        />
      }
      <RegistrationDialog
        open={dialogOpen}
        handleSubmit={handleSubmit}
        handleClose={handleClose}
      />
      <DeregistrationDialog
        open={deregistrationDialog.open}
        uuid={deregistrationDialog.uuid}
        password={deregistrationDialog.pass}
        handleSubmit={handleDeregistration}
        handleClose={handleDeregistrationClose}
      />
      <Fab
        sx={fabStyle}
        color="primary"
        aria-label="add"
        onClick={() => setDialogOpen(true)}
      >
        <AddIcon />
      </Fab>
    </>
  );
};

export { Registration };
