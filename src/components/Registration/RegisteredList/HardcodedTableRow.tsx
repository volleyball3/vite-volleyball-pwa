import { FC, ReactNode } from 'react';
import { ArrowDropDownCircle } from '@mui/icons-material';
import SportsVolleyballIcon from '@mui/icons-material/SportsVolleyball';

import { StyledTableCell, StyledTableRow } from './RegisteredList.styles';

type HardcodedTableRowProps = {
  text: string;
  icon: ReactNode;
  id: number;
};

const HardcodedTableRow: FC<HardcodedTableRowProps> = ({ text, icon, id }) => (
  <StyledTableRow key={id}>
    <StyledTableCell>{icon}</StyledTableCell>
    <StyledTableCell align="center" colSpan={2}>
      {text}
    </StyledTableCell>
    <StyledTableCell align="center">{icon}</StyledTableCell>
  </StyledTableRow>
);

const WaitlistDivider = () => (
  <HardcodedTableRow
    text="The waitlist begins here"
    icon={<ArrowDropDownCircle />}
    id={18}
  />
);

const ListPlaceholder = () => (
  <HardcodedTableRow
    text="No Registrations yet"
    icon={<SportsVolleyballIcon />}
    id={20}
  />
);

export { ListPlaceholder, WaitlistDivider };
