import { styled } from '@mui/material/styles';
import {
  Table,
  TableBody,
  TableCell,
  TableRow,
  tableCellClasses,
} from '@mui/material';

const StyledTableRow = styled(TableRow)(() => ({
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
  borderBottom: 'none',
}));

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const WaitlistTableRow = styled(TableRow)(({ theme }) => ({
  backgroundColor: `${theme.palette.error.light}`,
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
  borderBottom: 'none',
}));

const StyledTable = styled(Table)(() => ({
  borderBottom: 'none',
  '& tbody tr:last-child th, & tbody tr:last-child td': {
    border: 0,
  },
  maxWidth: 700,
  marginLeft: '.5rem',
  marginRight: '.5rem',
  marginBottom: '5rem',
  paddingBottom: '5rem',
}));

const StyledTableBody = styled(TableBody)(() => ({
  borderBottom: 'none',
  '& tbody tr:last-child th, & tbody tr:last-child td': {
    border: 0,
  },
}));

export {
  StyledTableCell,
  StyledTableRow,
  WaitlistTableRow,
  StyledTable,
  StyledTableBody,
};
