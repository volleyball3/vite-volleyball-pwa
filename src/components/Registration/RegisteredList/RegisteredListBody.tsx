import formatISO9075 from 'date-fns/formatISO9075';
import { Button } from '@mui/material';
import { DeleteForever } from '@mui/icons-material';
import { DocumentData } from 'firebase/firestore';
import {
  StyledTableBody,
  StyledTableRow,
  StyledTableCell,
  WaitlistTableRow,
} from './RegisteredList.styles';
import { ListPlaceholder, WaitlistDivider } from './HardcodedTableRow';
import { Fragment } from 'react';

type RegisteredListBodyProps = {
  registeredList: {
    data: DocumentData;
    id: string;
  }[];
  handleDeregistrationClick: (uuid: string, pass: string) => void;
};

const RegisteredListBody = ({
  registeredList,
  handleDeregistrationClick,
}: RegisteredListBodyProps) => {
  return (
    <StyledTableBody>
      {registeredList.map((row, i) => {
        const Row = i < 18 ? StyledTableRow : WaitlistTableRow;
        return row.id && row.data.createdAt ? (
          <Fragment key={row.id}>
            {i === 18 && <WaitlistDivider />}
            <Row
              key={row.id}
              sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
              <StyledTableCell>{i + 1}</StyledTableCell>
              <StyledTableCell align="left">{row.data.name}</StyledTableCell>
              <StyledTableCell align="left">
                {formatISO9075(row.data.createdAt.seconds * 1000)}
              </StyledTableCell>
              <StyledTableCell align="center">
                <Button
                  onClick={() =>
                    handleDeregistrationClick(row.id, row.data.pass)
                  }
                  color={i < 18 ? 'primary' : 'inherit'}
                >
                  <DeleteForever />
                </Button>
              </StyledTableCell>
            </Row>
          </Fragment>
        ) : null;
      })}
      {registeredList && registeredList.length === 0 && <ListPlaceholder />}
    </StyledTableBody>
  );
};

export { RegisteredListBody };
