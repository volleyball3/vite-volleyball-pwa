import { Paper, TableContainer, TableHead, TableRow } from '@mui/material';
import { DocumentData } from 'firebase/firestore';
import { StyledTable, StyledTableCell } from './RegisteredList.styles';
import { RegisteredListBody } from './RegisteredListBody';

type RegisteredListProps = {
  registeredList: {
    data: DocumentData;
    id: string;
  }[];
  handleDeregistrationClick: (uuid: string, pass: string) => void;
};

const RegisteredList = ({
  registeredList,
  handleDeregistrationClick,
}: RegisteredListProps) => {
  if (import.meta.env.DEV) {
    console.log('RegisteredList -> list: ', registeredList);
  }

  return (
    <TableContainer component={Paper}>
      <StyledTable size="small">
        <TableHead>
          <TableRow key="header">
            <StyledTableCell>No</StyledTableCell>
            <StyledTableCell align="left">Name</StyledTableCell>
            <StyledTableCell align="left">Time</StyledTableCell>
            <StyledTableCell align="center">Deregister</StyledTableCell>
          </TableRow>
        </TableHead>
        <RegisteredListBody
          registeredList={registeredList}
          handleDeregistrationClick={handleDeregistrationClick}
        />
      </StyledTable>
    </TableContainer>
  );
};

export { RegisteredList };
