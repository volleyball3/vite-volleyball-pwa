import {
  DocumentData,
  collection,
  deleteDoc,
  doc,
  getDoc,
  onSnapshot,
  orderBy,
  query,
  serverTimestamp,
  setDoc,
} from 'firebase/firestore';
import { useEffect, useState } from 'react';
import formatISO from 'date-fns/formatISO';
import { v4 as uuid } from 'uuid';

import { getRegistrationStartDateTimeThisWeek } from '../../utils/helpers';
import { firestoreInstance } from '../../lib/init-firebase';

const useFirestoreCollection = () => {
  const [registeredList, setRegisteredList] = useState<
    { data: DocumentData; id: string }[]
  >([]);
  const collectionName = import.meta.env.PROD
    ? formatISO(getRegistrationStartDateTimeThisWeek())
    : 'dev';
  const RegisteredListRef = collection(firestoreInstance, collectionName);
  const DeregisteredListRef = collection(
    firestoreInstance,
    `${collectionName}_dereg`,
  );

  useEffect(() => {
    const registeredQuery = query(
      RegisteredListRef,
      orderBy('createdAt', 'asc'),
    );

    const unsub = onSnapshot(registeredQuery, querySnapshot => {
      const list: { data: DocumentData; id: string }[] = [];
      querySnapshot.forEach(doc => {
        list.push({ data: doc.data(), id: doc.id });
      });
      setRegisteredList(list);
    });
    return () => {
      unsub();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const registerPlayer = async (name: string, pass: string) => {
    const newId = uuid();
    const userRef = doc(RegisteredListRef, newId);
    await setDoc(userRef, {
      id: newId,
      name,
      pass,
      createdAt: serverTimestamp(),
      lastUpdated: serverTimestamp(),
    });
  };

  const deregisterPlayer = async (uuid: string, pass: string) => {
    const userRef = doc(RegisteredListRef, uuid);
    const userData = getDoc(userRef);

    const newUserRef = doc(DeregisteredListRef, uuid);
    await setDoc(newUserRef, {
      ...(await userData).data(),
      lastUpdated: serverTimestamp(),
    });

    await deleteDoc(userRef);
  };

  return {
    RegisteredListRef,
    registeredList,
    registerPlayer,
    deregisterPlayer,
  };
};

export { useFirestoreCollection };
