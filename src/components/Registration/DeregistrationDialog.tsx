import { Visibility, VisibilityOff } from '@mui/icons-material';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  IconButton,
  Input,
  InputAdornment,
  InputLabel,
} from '@mui/material';
import { useState } from 'react';

type DeregistrationDialogProps = {
  open: boolean;
  uuid: string;
  password: string;
  handleSubmit: (uuid: string, pass: string) => void;
  handleClose: () => void;
};

const DeregistrationDialog = ({
  open,
  uuid,
  password,
  handleSubmit,
  handleClose,
}: DeregistrationDialogProps) => {
  const [pass, setPass] = useState('');
  const [passError, setPassError] = useState(false);
  const [showPassword, setShowPassword] = useState(false);
  const validate = () => {
    if (pass === password) {
      handleSubmit(uuid, pass);
    } else {
      setPassError(true);
    }
  };

  const handleClickShowPassword = () => setShowPassword(show => !show);

  const handleMouseDownPassword = (
    event: React.MouseEvent<HTMLButtonElement>,
  ) => {
    event.preventDefault();
  };

  return (
    <Dialog onClose={handleClose} open={open}>
      <DialogTitle>Deregister</DialogTitle>
      <DialogContent>
        <FormControl sx={{ mx: 2, width: '25ch' }} variant="standard">
          <InputLabel htmlFor="standard-adornment-password">
            Password
          </InputLabel>
          <Input
            required
            id="standard-adornment-password"
            type={showPassword ? 'text' : 'password'}
            error={passError}
            value={pass}
            onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
              setPassError(false);
              setPass(event.target.value);
            }}
            autoComplete="current-password"
            endAdornment={
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={handleClickShowPassword}
                  onMouseDown={handleMouseDownPassword}
                >
                  {showPassword ? <VisibilityOff /> : <Visibility />}
                </IconButton>
              </InputAdornment>
            }
          />
        </FormControl>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClose}>Cancel</Button>
        <Button onClick={() => validate()}>Deregister!</Button>
      </DialogActions>
    </Dialog>
  );
};

export { DeregistrationDialog };
