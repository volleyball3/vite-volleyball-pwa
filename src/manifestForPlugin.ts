import { VitePWAOptions } from 'vite-plugin-pwa';

export const manifestForPlugin = (
  BASE_URL: string,
): Partial<VitePWAOptions> => ({
  registerType: 'autoUpdate',
  includeAssets: [
    'favicon.ico',
    'apple-touch-icon-180x180.png',
    'pwa-512x512.png',
    'pwa-192x192.png',
    'pwa-64x64.png',
    'maskable-icon-512x512.png',
  ],
  manifest: {
    name: 'COE Volleyball',
    short_name: 'COE Volleyball',
    description:
      'Volleyball at Kapellenstraße 36, 96117 Memmelsdorf on Thursdays.',
    icons: [
      {
        src: `${BASE_URL}pwa-192x192.png`,
        sizes: '192×192',
        type: 'image/png',
      },
      {
        src: `${BASE_URL}pwa-512x512.png`,
        sizes: '512×512',
        type: 'image/png',
      },
      {
        src: `${BASE_URL}apple-touch-icon-180x180.png`,
        sizes: '180X180',
        type: 'image/png',
        purpose: 'apple touch icon',
      },
      {
        src: `${BASE_URL}maskable-icon-512x512.png`,
        sizes: '512x512',
        type: 'image/png',
        purpose: 'any maskable',
      },
      {
        src: `${BASE_URL}pwa-64x64.png`,
        sizes: '64x64',
        type: 'image/png',
      },
    ],
    theme_color: '#1976d2',
    background_color: '#fff',
    display: 'standalone',
    start_url: '/',
    orientation: 'portrait',
  },
  devOptions: {
    enabled: true,
  },
  scope: '/',
});
