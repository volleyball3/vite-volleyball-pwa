import { MenuEntry } from '../utils/types';
import { Registration } from '../components/Registration';
import { Home, Shuffle } from '@mui/icons-material';
import { Randomizer } from '../components/Randomizer/Randomizer';

export const menuEntries: MenuEntry[] = [
  {
    name: 'Registration',
    divider: false,
    path: '',
    icon: Home,
    component: Registration,
  },
  {
    name: 'Team Randomizer',
    divider: false,
    path: '/randomizer',
    icon: Shuffle,
    component: Randomizer,
  },
];
