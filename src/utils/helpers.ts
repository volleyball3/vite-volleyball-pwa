import setDay from 'date-fns/setDay';
import setHours from 'date-fns/setHours';
import setMinutes from 'date-fns/setMinutes';
import startOfMinute from 'date-fns/startOfMinute';
import add from 'date-fns/add';
import isAfter from 'date-fns/isAfter';
import isBefore from 'date-fns/isBefore';
import formatISO from 'date-fns/formatISO';
import { RegistrationStatus } from './types';

export const isValid = (value: string | undefined | null) =>
  value !== null && value !== undefined && value !== '';

export const castStringEnvVar = (value: string | undefined | null) =>
  value !== null && value !== undefined ? value : '';

export const getRegistrationStartDateTimeThisWeek = () =>
  startOfMinute(
    setMinutes(
      setHours(
        setDay(new Date(), import.meta.env.VITE_REG_START_DAY),
        import.meta.env.VITE_REG_START_HOUR,
      ),
      import.meta.env.VITE_REG_START_MINUTES,
    ),
  );

export const getIsRegistrationOpen = (): RegistrationStatus => {
  const now = new Date();
  const registrationStartDateTimeThisWeek =
    getRegistrationStartDateTimeThisWeek();
  const registrationEndDateTimeThisWeek = setMinutes(
    setHours(
      add(registrationStartDateTimeThisWeek, { days: 1 }),
      import.meta.env.VITE_REG_END_HOUR,
    ),
    import.meta.env.VITE_REG_END_MINUTES,
  );
  const holidays = import.meta.env.VITE_HOLIDAYS.split(',');
  let isRegistrationOpen: RegistrationStatus = 'Closed';
  if (holidays.includes(formatISO(registrationStartDateTimeThisWeek))) {
    isRegistrationOpen = 'Holiday';
  } else if (
    isAfter(now, registrationStartDateTimeThisWeek) &&
    isBefore(now, registrationEndDateTimeThisWeek)
  ) {
    isRegistrationOpen = 'Open';
  }

  return isRegistrationOpen;
};

export const log = (...args: any) => {
  if (import.meta.env.DEV) {
    console.log(...args);
  }
};
