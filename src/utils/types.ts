type RegistrationStatus = 'Open' | 'Closed' | 'Holiday';

type MenuEntry = {
  name: string;
  path: string;
  icon: any;
  divider: boolean;
  component: () => JSX.Element;
};

export type { MenuEntry, RegistrationStatus };
