# vite-volleyball-pwa

This mini project demonstrates skills in:

- React
- Typescript
- Gitlab CI
- Firebase - Firestore

In order to start this project you could click [here](https://volleyball3.gitlab.io/vite-volleyball-pwa/)
